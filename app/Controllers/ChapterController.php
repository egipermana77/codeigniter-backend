<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;
use Exception;
use App\Models\Chapter;
use App\Models\Courses;

class ChapterController extends BaseController
{
    public function index()
    {
        $model = new Chapter();

        $q = $this->request->getVar('q');
        if (isset($q)) {
            $model->groupStart()
                ->like('name', $q)
                ->groupEnd();
        }

        $where = $this->getParamsInput($this->request);
        $wherenya = array();
        if (isset($where)) {
            $wherenya = $this->loopSearch($where);
        }

        $model->where($wherenya);

        return $this->getResponse([
            'messages' => 'Data Chapters ditampilkan',
            'count' => $model->countAllResults(false),
            'data' => $model->paginate(5)
        ]);
    }

    public function Show($id)
    {
        try {

            $model = new Chapter();

            $chapter = $model->findChapterById($id);

            return $this->getResponse([
                'messages' => 'Data Chapter ditemukan',
                'data' => $chapter
            ]);
        } catch (Exception $e) {
            return $this->getResponse([
                'messages' => 'chapter dengan id tersebut tidak ditemukan'
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
    }

    public function store()
    {

        $rules = [
            'name' => 'required',
            'course_id' => 'required',
        ];
        $messages = [
            "course_id" => [
                "required" => "Course id tidak boleh kosong",
            ],
            "name" => [
                "required" => "Nama tidak boleh kosong",
            ],
        ];

        $input = $this->getRequestInput($this->request);
        if (!$this->validateRequest($input, $rules, $messages)) {
            return $this->getResponse(
                $this->validator->getErrors(),
                ResponseInterface::HTTP_BAD_REQUEST
            );
        }

        $courseid = $input['course_id'];
        $course = new Courses();


        if (!$course->find($courseid)) {
            return $this->getResponse([
                'messages' => 'course dengan id tersebut tidak ditemukan'
            ], ResponseInterface::HTTP_NOT_FOUND);
        }

        $model = new Chapter();
        $model->save($input);

        //tampilkan data
        $chapterID = $model->insertID();

        $chapter = $model->where('id', $chapterID)->first();

        return $this->getResponse([
            'messages' => 'Data Chapter berhasil ditambahkan',
            'data' => $chapter
        ]);
    }


    public function update($id)
    {
        try {

            $model = new Chapter();
            $model->findChapterById($id);

            $input = $this->getRequestInput($this->request);

            $model->update($id, $input);

            $chapter = $model->findChapterById($id);

            return $this->getResponse([
                'messages' => 'Data chapter berhasil diupdate',
                'data' => $chapter
            ]);
        } catch (Exception $e) {
            return $this->getResponse([
                'messages' => $e->getMessage()
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
    }

    public function destroy($id)
    {
        try {
            $model = new Chapter();

            $chapter = $model->findChapterById($id);

            $model->delete($chapter);

            return $this->getResponse([
                'messages' => 'Data chapter berhasil dihapus'
            ]);
        } catch (Exception $e) {
            return $this->getResponse([
                'messages' => $e->getMessage()
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
    }


    //batas
}
