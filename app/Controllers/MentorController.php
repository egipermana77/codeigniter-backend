<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;
use Exception;
use App\Models\Mentor;


class MentorController extends BaseController
{
    public function index()
    {
        $model = new Mentor();


        $q = $this->request->getVar('q');
        if (isset($q)) {
            $model->groupStart()
                ->like('name', $q)
                ->orLike('email', $q)
                ->groupEnd();
        }

        return $this->getResponse([
            'message' => 'Data mentor ditampilkan',
            'count' => $model->countAllResults(false),
            'data' => $model->paginate(5)
        ]);
    }

    public function store()
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|min_length[6]|max_length[50]|valid_email|is_unique[mentors.email]',
            'profile_photo' => 'is_image[photo_path]|mime_in[photo_path,image/jpg,image/jpeg,image/gif,image/png,image/webp]|max_size[photo_path,2048]'
        ];
        $messages = [
            "email" => [
                "required" => "Email tidak boleh kosong",
                "valid_email" => "Email tidak valid",
                "is_unique" => "Email sudah terdaftar",
            ],
            "name" => [
                "required" => "Nama tidak boleh kosong",
            ],
            'photo_path' => [
                "mime_in" => "File extension harus jpg jpeg gif png dan webp",
                "max_size" => "File maksimal 2 MB"
            ]
        ];

        $input = $this->getRequestInput($this->request);
        if (!$this->validateRequest($input, $rules, $messages)) {
            return $this->getResponse(
                $this->validator->getErrors(),
                ResponseInterface::HTTP_BAD_REQUEST
            );
        }

        $data = array();
        foreach ($input as $key => $val) {
            $data[$key] = $val;
        }

        $file = $this->request->getFile('photo_path');
        $name = $file->getRandomName();
        $file->move('uploads/mentors', $name);


        $data['photo_path'] = $name;

        $mentorEmail = $input['email'];

        $model = new Mentor();
        $model->save($data);

        $mentor = $model->where('email', $mentorEmail)->first();

        return $this->getResponse([
            'messages' => 'Data mentor berhasil ditambahkan',
            'data' => $mentor
        ]);
    }

    public function show($id)
    {
        try {

            $model = new Mentor();
            $mentor = $model->findMentorById($id);

            return $this->getResponse([
                'message' => 'Data mentor berhasil ditemukan',
                'data' => $mentor
            ]);
        } catch (Exception $e) {
            return $this->getResponse([
                'messages' => 'Mentor dengan id tersebut tidak ditemukan'
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
    }

    public function update($id)
    {

        try {

            $model = new Mentor();
            $model->findMentorById($id);
            $files = $model->findMentorById($id);

            $input = $this->getRequestInput($this->request);

            $data = array();
            foreach ($input as $key => $val) {
                $data[$key] = $val;
            }

            $file = $this->request->getFile('photo_path');
            if (isset($file)) {
                if ($files['photo_path'] && file_exists('uploads/mentors/' . $files['photo_path'])) {
                    unlink('uploads/mentors/' . $files['photo_path']);
                }
                $name = $file->getRandomName();
                $file->move('uploads/mentors', $name);
                $data['photo_path'] = $name;
            }

            $model->update($id, $data);

            $mentor = $model->findMentorById($id);


            return $this->getResponse([
                'messages' => 'Data client berhasil diupdate',
                'data' => $mentor
            ]);
        } catch (Exception $e) {
            return $this->getResponse([
                'messages' => $e->getMessage()
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
    }


    public function destroy($id)
    {
        try {

            $model = new Mentor();

            $mentor = $model->findMentorById($id);

            if ($mentor && $mentor['photo_path'] !== null) {
                unlink('uploads/mentors/' . $mentor['photo_path']);
            }

            $model->delete($mentor);


            return $this->getResponse([
                'messages' => 'Data mentor berhasil dihapus'
            ]);
        } catch (Exception $e) {
            return $this->getResponse([
                'messages' => $e->getMessage()
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
    }



    //batas
}
