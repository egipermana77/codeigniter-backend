<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\Courses;
use App\Models\Review;
use CodeIgniter\HTTP\ResponseInterface;
use Exception;

class ReviewController extends BaseController
{
    public function index()
    {
        $model = new Review();

        $where = $this->getParamsInput($this->request);
        $wherenya = array();
        if (isset($where)) {
            $wherenya = $this->loopSearch($where);
        }

        $model->where($wherenya);

        return $this->getResponse([
            'messages' => 'Data Review ditampilkan',
            'count' => $model->countAllResults(false),
            'data' => $model->paginate(5)
        ]);
    }

    public function show($id)
    {
        try {

            $model = new Review();
            $review = $model->findReviewById($id);

            return $this->getResponse([
                'message' => 'Data review ditemukan',
                'data' => $review
            ]);
        } catch (Exception $e) {
            return $this->getResponse([
                'messages' => 'Review dengan id tersebut tidak ditemukan'
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
    }

    public function store()
    {
        $rules = [
            'user_id' => 'required',
            'course_id' => 'required',
            'rating' => 'required|integer',
            'note' => 'required'
        ];
        $messages = [
            "course_id" => [
                "required" => "Course id tidak boleh kosong",
            ],
            "user_id" => [
                "user_id" => "User id tidak boleh kosong",
            ],
            "rating" => [
                "required" => "Rating tidak boleh kosong",
                "integer" => "harus berupa angka",
            ],
            "note" => [
                "required" => "Note tidak boleh kosong"
            ],
        ];

        $input = $this->getRequestInput($this->request);
        if (!$this->validateRequest($input, $rules, $messages)) {
            return $this->getResponse(
                $this->validator->getErrors(),
                ResponseInterface::HTTP_BAD_REQUEST
            );
        }

        $courseid = $input['course_id'];
        $course = new Courses();

        if (!$course->find($courseid)) {
            return $this->getResponse([
                'messages' => 'course dengan id tersebut tidak ditemukan'
            ], ResponseInterface::HTTP_NOT_FOUND);
        }

        $model = new Review();
        $model->save($input);

        $reviewID = $model->insertID();

        $review = $model->where('id', $reviewID)->first();

        return $this->getResponse([
            'messages' => 'Data review berhasil ditambahkan',
            'data' => $review
        ]);
    }

    public function update($id)
    {
        try {

            $model = new Review();
            $model->findReviewById($id);

            $input = $this->getRequestInput($this->request);

            $model->update($id, $input);

            $review = $model->findReviewById($id);

            return $this->getResponse([
                'messages' => 'Data review berhasil diupdate',
                'data' => $review
            ]);
        } catch (Exception $e) {
            return $this->getResponse([
                'messages' => $e->getMessage()
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
    }


    public function destroy($id)
    {
        try {

            $model = new Review();
            $review = $model->findReviewById($id);

            $model->delete($review);

            return $this->getResponse([
                'messages' => 'Data Review berhasil dihapus'
            ]);
        } catch (Exception $e) {
            return $this->getResponse([
                'message' => $e->getMessage()
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
    }

    //batas
}
