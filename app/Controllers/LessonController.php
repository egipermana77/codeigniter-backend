<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\Chapter;
use App\Models\Lessons;
use CodeIgniter\HTTP\ResponseInterface;
use Exception;

class LessonController extends BaseController
{
    public function index()
    {
        $model = new Lessons();

        $q = $this->request->getVar('q');
        if (isset($q)) {
            $model->groupStart()
                ->like('name', $q)
                ->groupEnd();
        }

        $where = $this->getParamsInput($this->request);
        $wherenya = array();
        if (isset($where)) {
            $wherenya = $this->loopSearch($where);
        }

        $model->where($wherenya);

        return $this->getResponse([
            'messages' => 'Data Lesson ditampilkan',
            'count' => $model->countAllResults(false),
            'data' => $model->paginate(5)
        ]);
    }


    public function show($id)
    {
        try {
            $model = new Lessons();
            $lesson = $model->findLessonById($id);

            return $this->getResponse([
                'messages' => 'Data Lesson ditemukan',
                'data' => $lesson
            ]);
        } catch (Exception $e) {
            return $this->getResponse([
                'messages' => 'lesson dengan id tersebut tidak ditemukan'
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
    }

    public function store()
    {
        $rules = [
            'name' => 'required',
            'video_url' => 'required',
            'chapters_id' => 'required',
        ];
        $messages = [
            "chapters_id" => [
                "required" => "chapter id tidak boleh kosong",
            ],
            "video_url" => [
                "required" => "video url tidak boleh kosong",
            ],
            "name" => [
                "required" => "Nama tidak boleh kosong",
            ],
        ];

        $input = $this->getRequestInput($this->request);
        if (!$this->validateRequest($input, $rules, $messages)) {
            return $this->getResponse(
                $this->validator->getErrors(),
                ResponseInterface::HTTP_BAD_REQUEST
            );
        }

        $chapterID = $input['chapters_id'];
        $chapter = new Chapter();


        if (!$chapter->find($chapterID)) {
            return $this->getResponse([
                'messages' => 'chpater dengan id tersebut tidak ditemukan'
            ], ResponseInterface::HTTP_NOT_FOUND);
        }

        $model = new Lessons();
        $model->save($input);

        //tampilkan data
        $lessonID = $model->insertID();

        $lesson = $model->where('id', $lessonID)->first();

        return $this->getResponse([
            'messages' => 'Data lesson berhasil ditambahkan',
            'data' => $lesson
        ]);
    }

    public function update($id)
    {
        try {
            $model = new Lessons();
            $model->findLessonById($id);

            $input = $this->getRequestInput($this->request);

            $model->update($id, $input);

            $lesson = $model->findLessonById($id);

            return $this->getResponse([
                'messages' => 'Data lesson berhasil diupdate',
                'data' => $lesson
            ]);
        } catch (Exception $e) {
            return $this->getResponse([
                'messages' => $e->getMessage()
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
    }

    public function destroy($id)
    {
        try {
            $model = new Lessons();

            $lesson = $model->findLessonById($id);

            $model->delete($lesson);

            return $this->getResponse([
                'messages' => 'Data lesson berhasil dihapus'
            ]);
        } catch (Exception $e) {
            return $this->getResponse([
                'messages' => $e->getMessage()
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
    }

    //batas
}
