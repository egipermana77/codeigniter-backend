<?php

namespace App\Controllers;

use App\Models\Chapter;
use App\Models\Lessons;

class Home extends BaseController
{
    public function index()
    {
        return view('welcome_message');
    }

    public function showChapter()
    {
        $data = array();
        $model = new Chapter();
        $data = $model->findCourseById(4);

        foreach ($data as $key => $val) {
            $id = $val['id'];
            $lesMode = new Lessons();
            $datales = $lesMode->findLessonByChapterId($id);

            $data[$key] = $val;
            $data[$key]['lesson'] = $datales;
        }

        dd($data);
    }
}
