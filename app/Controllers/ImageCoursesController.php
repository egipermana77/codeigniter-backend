<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\Courses;
use App\Models\ImageCourses;
use CodeIgniter\HTTP\ResponseInterface;
use Exception;

class ImageCoursesController extends BaseController
{
    public function index()
    {
        $model = new ImageCourses();

        $where = $this->getParamsInput($this->request);
        $wherenya = array();
        if (isset($where)) {
            $wherenya = $this->loopSearch($where);
        }

        $model->where($wherenya);

        return $this->getResponse([
            'messages' => 'Image Course ditampilkan',
            'count' => $model->countAllResults(false),
            'data' => $model->paginate(5)
        ]);
    }

    public function show($id)
    {
        try {

            $model = new ImageCourses();
            $mentor = $model->findImageById($id);

            return $this->getResponse([
                'message' => 'Data image berhasil ditemukan',
                'data' => $mentor
            ]);
        } catch (Exception $e) {
            return $this->getResponse([
                'messages' => 'Mentor dengan id tersebut tidak ditemukan'
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
    }


    public function store()
    {
        $rules = [
            'course_id' => 'required',
            'course_photo' => 'is_image[photo_path]|mime_in[photo_path,image/jpg,image/jpeg,image/gif,image/png,image/webp]|max_size[photo_path,2048]'
        ];
        $messages = [
            "course_id" => [
                "required" => "course id tidak boleh kosong",
            ],
            'photo_path' => [
                "mime_in" => "File extension harus jpg jpeg gif png dan webp",
                "max_size" => "File maksimal 2 MB"
            ]
        ];

        $input = $this->getRequestInput($this->request);
        if (!$this->validateRequest($input, $rules, $messages)) {
            return $this->getResponse(
                $this->validator->getErrors(),
                ResponseInterface::HTTP_BAD_REQUEST
            );
        }

        $courseID = $input['course_id'];
        $course = new Courses();

        if (!$course->find($courseID)) {
            return $this->getResponse([
                'messages' => 'course dengan id tersebut tidak ditemukan'
            ], ResponseInterface::HTTP_NOT_FOUND);
        }

        $data = array();
        foreach ($input as $key => $val) {
            $data[$key] = $val;
        }

        $file = $this->request->getFile('photo_path');
        $name = $file->getRandomName();
        $file->move('uploads/courses', $name);

        $data['image'] = $name;

        $model = new ImageCourses();
        $model->save($data);

        $imageID = $model->insertID();
        $imageCourse = $model->where('id', $imageID)->first();

        return $this->getResponse([
            'messages' => 'Data mentor berhasil ditambahkan',
            'data' => $imageCourse
        ]);
    }

    public function update($id)
    {
        try {

            $model = new ImageCourses();
            $model->findImageById($id);
            $files = $model->findImageById($id);

            $input = $this->getRequestInput($this->request);

            $data = array();
            foreach ($input as $key => $val) {
                $data[$key] = $val;
            }

            $file = $this->request->getFile('photo_path');
            if (isset($file)) {
                if ($files['image'] && file_exists('uploads/courses/' . $files['image'])) {
                    unlink('uploads/courses/' . $files['image']);
                }
                $name = $file->getRandomName();
                $file->move('uploads/courses', $name);
                $data['image'] = $name;
            }

            $model->update($id, $data);

            $image = $model->findImageById($id);


            return $this->getResponse([
                'messages' => 'Data image berhasil diupdate',
                'data' => $image
            ]);
        } catch (Exception $e) {
            return $this->getResponse([
                'messages' => $e->getMessage()
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
    }

    public function destroy($id)
    {
        try {
            $model = new ImageCourses();
            $image = $model->findImageById($id);

            if ($image && $image['image'] !== null) {
                unlink('uploads/courses/' . $image['photo_path']);
            }

            $model->delete($image);
            return $this->getResponse([
                'messages' => 'Data image berhasil dihapus'
            ]);
        } catch (Exception $e) {
            return $this->getResponse([
                'messages' => $e->getMessage()
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
    }

    //batas
}
