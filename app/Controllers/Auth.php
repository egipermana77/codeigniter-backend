<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\UserModel;
use CodeIgniter\HTTP\Response;
use CodeIgniter\HTTP\ResponseInterface;
use Exception;
use PhpParser\Node\Stmt\TryCatch;

class Auth extends BaseController
{
    /**
     * for generate JWT
     */

    private function getJWTForUser(string $emailAddress, int $responseCode = ResponseInterface::HTTP_OK)
    {
        try {
            $model = new UserModel();
            $user = $model->findUserByEmailAddress($emailAddress);
            unset($user['password']);

            helper('jwt');
            return $this->getResponse([
                'messages' => 'Authentifikasi user berhasil',
                'user' => $user,
                'access_token' => getSignedJWTFromRequest($emailAddress, $user['id'])
            ]);
        } catch (Exception $e) {
            return $this->getResponse([
                'error' => $e->getMessage()
            ], $responseCode);
        }
    }

    /**
     * for register user
     */

    public function register()
    {

        $rules = [
            'email' => 'required|valid_email|is_unique[users.email]',
            'name' => 'required',
            'password' => 'required|min_length[3]'
        ];

        $messages = [
            "email" => [
                "required" => "Email tidak boleh kosong",
                "valid_email" => "Email tidak valid",
                "is_unique" => "Email sudah terdaftar",
            ],
            "name" => [
                "required" => "Nama tidak boleh kosong",
            ],
            "password" => [
                "required" => "password tidak boleh kosong",
                "min_length" => "password minimal 3 karakter"
            ],
        ];

        $input = $this->getRequestInput($this->request);
        if (!$this->validateRequest($input, $rules, $messages)) {
            return $this->getResponse(
                $this->validator->getErrors(),
                ResponseInterface::HTTP_BAD_REQUEST
            );
        }

        $userModel = new UserModel();
        $userModel->save($input);

        return $this->getJWTForUser($input['email'], ResponseInterface::HTTP_CREATED);
    }


    /**
     * for login user
     */

    public function login()
    {
        $rules = [
            'email' => 'required|valid_email|validateEmail[email]',
            'password' => 'required|min_length[3]|validateUser[email, password]'
        ];
        $messages = [
            "email" => [
                "required" => "Email tidak boleh kosong",
                "valid_email" => "Email tidak valid",
                "validateEmail" => "Email belum terdaptar"
            ],
            "password" => [
                "required" => "password tidak boleh kosong",
                'validateUser' => 'Password yang anda masukan salah',
                "min_length" => "password minimal 3 karakter"
            ],
        ];

        $input = $this->getRequestInput($this->request);
        if (!$this->validateRequest($input, $rules, $messages)) {
            return $this->getResponse(
                $this->validator->getErrors(),
                ResponseInterface::HTTP_BAD_REQUEST
            );
        }
        return $this->getJWTForUser($input['email']);
    }


    public function uploadPhoto()
    {
        $token = $this->request->getServer('HTTP_AUTHORIZATION');

        $rules = [
            'userfile' => 'is_image[userfile]|mime_in[userfile,image/jpg,image/jpeg,image/gif,image/png,image/webp]|max_size[userfile,2048]'
        ];
        $messages = [
            "userfile" => [
                "is_image" => "Harus image yang diupload",
                "mime_in" => "File extension harus jpg jpeg gif png dan webp",
                "max_size" => "File maksimal 2 MB"
            ],
        ];

        try {

            $model = new UserModel();

            $files = $this->getRequestInput($this->request);
            if (!$this->validateRequest($files, $rules, $messages)) {
                return $this->getResponse(
                    $this->validator->getErrors(),
                    ResponseInterface::HTTP_BAD_REQUEST
                );
            }

            helper('jwt');
            $encodeToken = getJWTFromRequest($token);
            $decode = decodeJWTToken($encodeToken);
            $id = $decode->id;

            $user = $model->findUserById($id);
            unset($user['password']);

            $file = $this->request->getFile('userfile');

            //fungsi unlink
            if (!empty($file)) {
                unlink('uploads/profile/' . $user['photo_path']);
            }

            $name = $file->getRandomName();
            $file->move('uploads/profile', $name);

            $data = [
                'photo_path' => $name,
            ];

            $model->update($id, $data);

            return $this->getResponse([
                'message' => 'Photo profile berhasil di upload',
                'data' => $user,

            ]);
        } catch (Exception $e) {
            return $this->getResponse([
                'messages' => $e->getMessage()
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
    }


    /**
     * setting email send
     */

    private function getEmail($attachment = null, $to, $title, $messages)
    {
        $email = \Config\Services::email(); // loading for use
        try {
            $email->setFrom('egi.permana76@gmail.com', 'Developer Aplikasi');
            $email->setTo($to);

            $email->attach(FCPATH . $attachment);

            $email->setSubject($title);
            $email->setMessage($messages);
            $email->send();

            return $this->getResponse([
                'messages' => 'email telah terkirim'
            ]);
        } catch (Exception $e) {
            return $this->getResponse([
                'error' => $email->printDebugger(['headers'])
            ]);
        }
    }

    public function sendEmail()
    {
        $to = 'egicadangan@gmail.com';
        $title = 'PDF Attachment in mail';
        $attachment = 'uploads/file_arcgis.pdf';
        $messages = " <h1>Apakah ada file yang dikirim</h1> <br /> Berikut lampiran filenya";

        return $this->getEmail($attachment, $to, $title, $messages);
    }


    //batas
}
