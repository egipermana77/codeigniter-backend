<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\MyCourses;
use CodeIgniter\HTTP\ResponseInterface;
use Exception;

class MyCoursesController extends BaseController
{
    public function index()
    {
        $model = new MyCourses();

        $where = $this->getParamsInput($this->request);
        $wherenya = array();
        if (isset($where)) {
            $wherenya = $this->loopSearch($where);
        }

        $model->where($wherenya);

        return $this->getResponse([
            'messages' => 'Data Course ditampilkan',
            'count' => $model->countAllResults(false),
            'data' => $model->paginate(5)
        ]);
    }

    public function show($id)
    {
        try {
            $model = new MyCourses();
            $myCourses = $model->findMyCourseById($id);

            return $this->getResponse([
                'messages' => 'Data Mycourses ditemukan',
                'data' => $myCourses
            ]);
        } catch (Exception $e) {
            return $this->getResponse([
                'messages' => 'data mycourse dengan id tersebut tidak ditemukan'
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
    }


    public function store()
    {
        $rules = [
            "user_id" => "required|integer|isValidUser[user_id]",
            "course_id" => "required|integer|isValidCourse[course_id]"
        ];
        $messages = [
            "user_id" => [
                "required" => "user id tidak boleh kosong",
                "integer" => "harus id valid",
                "isValidUser" => "user id tersebut tidak ditemukan"
            ],
            "course_id" => [
                "required" => "course id tidak boleh kosong",
                "integer" => "harus id valid",
                "isValidCourse" => "course id tersebut tidak ditemukan"
            ],
        ];

        $input = $this->getRequestInput($this->request);
        if (!$this->validateRequest($input, $rules, $messages)) {
            return $this->getResponse(
                $this->validator->getErrors(),
                ResponseInterface::HTTP_BAD_REQUEST
            );
        }

        $model = new MyCourses();


        if ($model->findDoubleCourse($input)) {
            return $this->getResponse([
                'messages' => 'user sudah mengambil course ini'
            ], ResponseInterface::HTTP_NOT_ACCEPTABLE);
        }

        $model->save($input);

        //tampilkan data
        $myCourseID = $model->insertID();

        $MyCourse = $model->where('id', $myCourseID)->first();

        return $this->getResponse([
            'messages' => 'Data MyCourse berhasil ditambahkan',
            'data' => $MyCourse
        ]);
    }

    public function update($id)
    {
        try {
            $model = new MyCourses();

            $model->findMyCourseById($id);

            $input = $this->getRequestInput($this->request);

            if ($model->findDoubleCourse($input)) {
                return $this->getResponse([
                    'messages' => 'user sudah mengambil course ini'
                ], ResponseInterface::HTTP_NOT_ACCEPTABLE);
            }

            $model->update($id, $input);

            $mycourse = $model->findMyCourseById($id);

            return $this->getResponse([
                'messages' => 'Data mycourse berhasil diupdate',
                'data' => $mycourse
            ]);
        } catch (Exception $e) {
            return $this->getResponse([
                'messages' => $e->getMessage()
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
    }

    public function destroy($id)
    {
        try {
            $model = new MyCourses();

            $mycourse = $model->findMyCourseById($id);

            $model->delete($mycourse);

            return $this->getResponse([
                'messages' => 'Data course berhasil dihapus'
            ]);
        } catch (Exception $e) {
            return $this->getResponse([
                'messages' => $e->getMessage()
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
    }

    //batas
}
