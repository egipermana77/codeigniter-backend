<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;
use Exception;
use App\Models\Courses;
use App\Models\Review;

class CorsesController extends BaseController
{
    public function index()
    {
        $model = new Courses();

        $q = $this->request->getVar('q');
        if (isset($q)) {
            $model->groupStart()
                ->like('name', $q)
                ->orLike('deskripsi', $q)
                ->orLike('thumbnail', $q)
                ->groupEnd();
        }


        $where = $this->getParamsInput($this->request);
        $wherenya = array();
        if (isset($where)) {
            $wherenya = $this->loopSearch($where);
        }


        $model->where($wherenya);

        return $this->getResponse([
            'messages' => 'Data Courses ditampilkan',
            'count' => $model->countAllResults(false),
            'data' => $model->paginate(5)
        ]);
    }

    public function show($id)
    {
        try {

            $data = array();
            $model = new Courses();

            $course = $model->findCourseById($id);

            $data = $this->loopSearch($course);

            $review = $model->findReview($id);
            $total_student = $model->totalStudent($id);
            $chapters = $model->findChapter($id);
            $mentor = $model->findMentor($course['mentor_id']);
            $images = $model->findImage($id);

            $data['review'] = $review;
            $data['total_student'] = $total_student;
            $data['chapters'] = $chapters;
            $data['mentor'] = $mentor;
            $data['images'] = $images;


            return $this->getResponse([
                'messages' => 'Data courses ditemukan',
                'data' => $data
            ]);
        } catch (Exception $e) {
            return $this->getResponse([
                'messages' => 'Course dengan id tersebut tidak ditemukan'
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
    }

    public function store()
    {
        $rules = [
            'name' => 'required',
            'sertifikat' => 'required|in_list[0,1]',
            'thumbnail' => 'required',
            'type' => 'required|in_list[free,premium]',
            'status' => 'required|in_list[draft,published]',
            'level' => 'required|in_list[all-level,beginer,intermediate,advance]',
            'price' => 'required|decimal',
            'mentor_id' =>  'required|integer',
        ];
        $messages = [
            "name" => [
                "required" => "name tidak boleh kosong",
            ],
            "sertifikat" => [
                "required" => "Sertifikat tidak boleh kosong",
                "in_list" => "Harus 1 atau 0"
            ],
            "thumbnail" => [
                "required" => "thumbnail tidak boleh kosong",
            ],
            "type" => [
                "required" => "type tidak boleh kosong",
                "in_list" => "harus free atau premium"
            ],
            "status" => [
                "required" => "status tidak boleh kosong",
                "in_list" => "harus draft atau published"
            ],
            "level" => [
                "required" => "level tidak boleh kosong",
                "in_list" => "harus diantara all-level,beginer,intermediate,advance"
            ],
            "price" => [
                "required" => "price tidak boleh kosong",
                "decimal" => "harus angka desimal"
            ],
            "mentor_id" => [
                "required" => "mentor_id tidak boleh kosong",
                "integer" => "harus angka"
            ]
        ];

        $input = $this->getRequestInput($this->request);
        if (!$this->validateRequest($input, $rules, $messages)) {
            return $this->getResponse(
                $this->validator->getErrors(),
                ResponseInterface::HTTP_BAD_REQUEST
            );
        }

        $model = new Courses();
        $model->save($input);

        //tampilkan data setelah berhasil input
        $courseID = $model->insertID();

        $course = $model->where('id', $courseID)->first();

        return $this->getResponse([
            'messages' => 'Data courses berhasil ditambahkan',
            'data' => $course
        ]);
    }

    public function update($id)
    {
        try {

            $model = new Courses();
            $model->findCourseById($id);

            $input = $this->getRequestInput($this->request);

            $model->update($id, $input);

            $course = $model->findCourseById($id);

            return $this->getResponse([
                'messages' => 'Data course berhasil diupdate',
                'data' => $course
            ]);
        } catch (Exception $e) {
            return $this->getResponse([
                'messages' => $e->getMessage()
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
    }

    public function destroy($id)
    {
        try {
            $model = new Courses();

            $course = $model->findCourseById($id);

            $model->delete($course);

            return $this->getResponse([
                'messages' => 'Data course berhasil dihapus'
            ]);
        } catch (Exception $e) {
            return $this->getResponse([
                'messages' => $e->getMessage()
            ], ResponseInterface::HTTP_NOT_FOUND);
        }
    }


    //batas
}
