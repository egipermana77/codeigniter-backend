<?php

namespace App\Models;

use CodeIgniter\Model;
use Exception;

class Courses extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'courses';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = [
        'name', 'sertifikat', 'thumbnail', 'type', 'status', 'level', 'price', 'deskripsi', 'mentor_id'
    ];

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    // protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public function findCourseById($id)
    {
        $course = $this
            ->asArray()
            ->where(['id' => $id])
            ->first();

        if (!$course) throw new Exception('tidak menemukan id spesifik');

        return $course;
    }

    public function findReview($courseid)
    {
        $review = new Review();
        $find = $review->findReviewByCoursedId($courseid);
        return $find;
    }

    public function totalStudent($courseid)
    {
        $myCourse = new MyCourses();
        $myCourse->where('course_id', $courseid);
        $total = $myCourse->countAllResults();
        return $total;
    }

    public function findChapter($courseid)
    {
        $chapter = new Chapter();
        $find = $chapter->findCourseById($courseid);
        return $find;
    }

    public function findMentor($mentorid)
    {
        $model = new Mentor();
        $mentor = $model->where('id', $mentorid)->findAll();
        return $mentor;
    }

    public function findImage($courseid)
    {
        $model = new ImageCourses();
        $find = $model->findImageByCourseId($courseid);
        return $find;
    }



    //batas
}
