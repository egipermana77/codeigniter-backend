<?php

namespace App\Models;

use CodeIgniter\Model;
use Exception;

class MyCourses extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'my_courses';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = [
        'course_id', 'user_id'
    ];

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    // protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public function findMyCourseById($id)
    {
        $myCourse = $this
            ->asArray()
            ->where(['id' => $id])
            ->first();
        if (!$myCourse) throw new Exception('tidak menemukan id spesifik');
        return $myCourse;
    }

    public function findDoubleCourse(array $data)
    {
        $where = array();
        foreach ($data as $key => $val) {
            $where[$key] = $val;
        }
        $myCourse = $this
            ->asArray()
            ->where($where)
            ->first();
        if ($myCourse) {
            return true;
        }
        return false;
    }
}
