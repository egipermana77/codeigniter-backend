<?php

namespace App\Models;

use CodeIgniter\Model;
use Exception;

class ImageCourses extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'image_courses';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = [
        'course_id', 'image'
    ];

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    // protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];


    public function findImageById($id)
    {
        $image = $this
            ->asArray()
            ->where(['id' => $id])
            ->first();

        if (!$image) throw new Exception('Tidak menemukan id spesifik');

        return $image;
    }

    public function findImageByCourseId($courseid)
    {
        $image = $this
            ->asArray()
            ->where(['course_id' => $courseid])->findAll();
        return $image;
    }
}
