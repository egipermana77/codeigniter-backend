<?php

namespace App\Models;

use CodeIgniter\Model;
use Exception;

class Lessons extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'lessons';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = [
        'name', 'video_url', 'chapters_id'
    ];

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    // protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public function findLessonById($id)
    {
        $lesson = $this
            ->asArray()
            ->where(['id' => $id])
            ->first();

        if (!$lesson) throw new Exception('tidak menemukan id spesifik');

        return $lesson;
    }

    public function findLessonByChapterId($chapterid)
    {
        $lesson = $this
            ->asArray()
            ->where(['chapters_id' => $chapterid])->findAll();
        return $lesson;
    }
}
