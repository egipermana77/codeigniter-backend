<?php

namespace App\Models;

use CodeIgniter\Model;
use Exception;

class Review extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'reviews';
    protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = [
        'user_id', 'course_id', 'rating', 'note', 'updated_at'
    ];

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    // protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];


    public function findReviewById($id)
    {
        $review = $this
            ->asArray()
            ->where(['id' => $id])
            ->first();

        if (!$review) throw new Exception('tidak menemukan id spesifik');

        return $review;
    }

    public function findReviewByCoursedId($courseid)
    {
        $review = $this
            ->asArray()
            ->where(['course_id' => $courseid])->findAll();
        return $review;
    }
}
