<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');
$routes->get('/chapter', 'Home::showChapter');


$routes->group('auth', function ($routes) {
    $routes->post('register', 'Auth::register');
    $routes->post('login', 'Auth::login');
});

$routes->group('api', function ($routes) {
    $routes->group('clients', ['filter' => 'JWTAuth'], function ($routes) {
        $routes->get('', 'Clients::index');
        $routes->post('', 'Clients::store');
        $routes->get('(:num)', 'Clients::show/$1');
        $routes->post('(:num)', 'Clients::update/$1');
        $routes->delete('(:num)', 'Clients::destroy/$1');
    });

    $routes->group('mentors', ['filter' => 'JWTAuth'], function ($routes) {
        $routes->get('', 'MentorController::index');
        $routes->post('', 'MentorController::store');
        $routes->get('(:num)', 'MentorController::show/$1');
        $routes->post('(:num)', 'MentorController::update/$1');
        $routes->delete('(:num)', 'MentorController::destroy/$1');
    });

    $routes->group('courses', ['filter' => 'JWTAuth'], function ($routes) {
        $routes->get('', 'CorsesController::index');
        $routes->post('', 'CorsesController::store');
        $routes->get('(:num)', 'CorsesController::show/$1');
        $routes->post('(:num)', 'CorsesController::update/$1');
        $routes->delete('(:num)', 'CorsesController::destroy/$1');
    });

    $routes->group('chapter', ['filter' => 'JWTAuth'], function ($routes) {
        $routes->get('', 'ChapterController::index');
        $routes->post('', 'ChapterController::store');
        $routes->get('(:num)', 'ChapterController::show/$1');
        $routes->post('(:num)', 'ChapterController::update/$1');
        $routes->delete('(:num)', 'ChapterController::destroy/$1');
    });

    $routes->group('lesson', ['filter' => 'JWTAuth'], function ($routes) {
        $routes->get('', 'LessonController::index');
        $routes->post('', 'LessonController::store');
        $routes->get('(:num)', 'LessonController::show/$1');
        $routes->post('(:num)', 'LessonController::update/$1');
        $routes->delete('(:num)', 'LessonController::destroy/$1');
    });

    $routes->group('review', ['filter' => 'JWTAuth'], function ($routes) {
        $routes->get('', 'ReviewController::index');
        $routes->post('', 'ReviewController::store');
        $routes->get('(:num)', 'ReviewController::show/$1');
        $routes->post('(:num)', 'ReviewController::update/$1');
        $routes->delete('(:num)', 'ReviewController::destroy/$1');
    });


    $routes->group('imagecourse', ['filter' => 'JWTAuth'], function ($routes) {
        $routes->get('', 'ImageCoursesController::index');
        $routes->post('', 'ImageCoursesController::store');
        $routes->get('(:num)', 'ImageCoursesController::show/$1');
        $routes->post('(:num)', 'ImageCoursesController::update/$1');
        $routes->delete('(:num)', 'ImageCoursesController::destroy/$1');
    });

    $routes->group('mycourse', ['filter' => 'JWTAuth'], function ($routes) {
        $routes->get('', 'MyCoursesController::index');
        $routes->post('', 'MyCoursesController::store');
        $routes->get('(:num)', 'MyCoursesController::show/$1');
        $routes->post('(:num)', 'MyCoursesController::update/$1');
        $routes->delete('(:num)', 'MyCoursesController::destroy/$1');
    });


    $routes->post('uploadPhoto', 'Auth::uploadPhoto');
});

$routes->get('sendEmail', 'Auth::sendEmail');



/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
