<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use Faker\Factory;

class ReviewSeed extends Seeder
{
    public function run()
    {
        for ($i = 0; $i < 10; $i++) {
            $this->db->table('reviews')->insert($this->generateReview());
        }
    }

    public function generateReview()
    {
        $faker = Factory::create();
        return [
            'user_id' => $faker->randomElement([1, 2]),
            'course_id' => $faker->randomElement([2, 3, 4, 5, 15, 20, 21, 16, 12, 8, 9]),
            'rating' => $faker->randomElement([1, 2, 3, 4, 5]),
            'note' => $faker->paragraph(),
        ];
    }
}
