<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use Faker\Factory;

class LessonSeeder extends Seeder
{
    public function run()
    {
        for ($i = 0; $i < 10; $i++) {
            $this->db->table('lessons')->insert($this->generateLesson());
        }
    }

    public function generateLesson()
    {
        $faker = Factory::create();
        return [
            'name' => $faker->name,
            'video_url' => $faker->slug(3),
            'chapters_id' => $faker->randomElement([2, 3, 4, 5, 6, 7, 8, 9, 10]),
        ];
    }
}
