<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use Faker\Factory;

class ChapterSeeder extends Seeder
{
    public function run()
    {
        for ($i = 0; $i < 10; $i++) {
            $this->db->table('chapters')->insert($this->generateChapter());
        }
    }

    public function generateChapter()
    {
        $faker = Factory::create();
        return [
            'name' => $faker->name,
            'course_id' => $faker->randomElement([1, 2, 3, 4, 5, 15])
        ];
    }
}
