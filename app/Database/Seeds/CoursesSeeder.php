<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use Faker\Factory;

class CoursesSeeder extends Seeder
{
    public function run()
    {
        for ($i = 0; $i < 20; $i++) { //to add 10 clients. Change limit as desired
            $this->db->table('courses')->insert($this->generateCourses());
        }
    }

    public function generateCourses()
    {
        $faker = Factory::create();
        return [
            'name' => $faker->name(),
            'sertifikat' => $faker->boolean(),
            'thumbnail' => $faker->text(100),
            'type' => $faker->randomElement(['free', 'premium']),
            'status' => $faker->randomElement(['draft', 'published']),
            'level' => $faker->randomElement(['all-level', 'beginer', 'intermediate', 'advance']),
            'price' => $faker->randomDigit(),
            'deskripsi' => $faker->paragraph(2),
            'mentor_id' => $faker->randomElement([1, 2, 3, 4, 5, 15])
        ];
    }
}
