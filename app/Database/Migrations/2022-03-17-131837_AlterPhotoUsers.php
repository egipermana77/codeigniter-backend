<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AlterPhotoUsers extends Migration
{
    public function up()
    {
        $fields = [
            'email_verified_at' => [
                'type' => 'timestamp',
                'after' => 'email'
            ],
            'photo_path' => [
                'type' => 'varchar',
                'constraint' => 255,
                'after' => 'password'
            ]
        ];
        $this->forge->addColumn('users', $fields);
    }

    public function down()
    {
        //
    }
}
