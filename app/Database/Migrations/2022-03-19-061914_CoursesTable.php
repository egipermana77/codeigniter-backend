<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CoursesTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => '255',
            ],
            'sertifikat' => [
                'type' => 'BOOLEAN',
            ],
            'thumbnail' => [
                'type' => 'VARCHAR',
                'constraint' => '255',
            ],
            'type' => [
                'type' => 'ENUM',
                'constraint' =>  ['free', 'premium'],
                'default' => 'premium'
            ],
            'status' => [
                'type' => 'ENUM',
                'constraint' =>  ['draft', 'published'],
                'default' => 'draft'
            ],
            'level' => [
                'type' => 'ENUM',
                'constraint' =>  ['all-level', 'beginer', 'intermediate', 'advance'],
                'default' => 'all-level'
            ],
            'price' => [
                'type' => 'DECIMAL',
                'constraint' => '10,2',

            ],
            'deskripsi' => [
                'type' => 'TEXT',
                'null' => true,
            ],
            'mentor_id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
            ],
            'updated_at' => [
                'type' => 'datetime',
                'null' => true,
            ],
            'created_at datetime default current_timestamp',
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->addForeignKey('mentor_id', 'mentors', 'id', 'CASCADE');
        $this->forge->createTable('courses');
    }

    public function down()
    {
        $this->forge->dropTable('courses');
    }
}
