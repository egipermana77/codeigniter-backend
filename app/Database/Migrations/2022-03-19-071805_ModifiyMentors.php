<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class ModifiyMentors extends Migration
{
    public function up()
    {
        $fields = [
            'profile' => [
                'name' => 'photo_path',
                'type' => 'VARCHAR',
                'constraint' => '255',
            ],
        ];
        $this->forge->modifyColumn('mentors', $fields);
    }

    public function down()
    {
        //
    }
}
