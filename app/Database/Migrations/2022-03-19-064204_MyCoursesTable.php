<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class MyCoursesTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'course_id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
            ],
            'user_id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => true,
            ],
            'updated_at' => [
                'type' => 'datetime',
                'null' => true,
            ],
            'created_at datetime default current_timestamp',
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->addForeignKey('course_id', 'courses', 'id', 'CASCADE');
        $this->forge->addKey(['course_id', 'user_id']);
        $this->forge->createTable('my_courses');
    }

    public function down()
    {
        $this->forge->dropTable('my_courses');
    }
}
