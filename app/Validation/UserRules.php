<?php

namespace App\Validation;

use App\Models\Courses;
use App\Models\UserModel;
use Exception;

/**
 * tambahan untuk validasi user
 */

class UserRules
{
    public function validateUser(string $str, string $fields, array $data)
    {
        try {
            $model = new UserModel();
            $user = $model->findUserByEmailAddress($data['email']);
            return password_verify($data['password'], $user['password']);
        } catch (Exception $e) {
            return false;
        }
    }

    public function validateEmail(string $str, string $fields, array $data)
    {
        try {
            $model = new UserModel();
            $user = $model->findUserByEmailAddress($data['email']);
            if ($user) return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function isValidUser(string $str, string $fields, array $data)
    {
        try {
            $model = new UserModel();
            $user = $model->findUserById($data['user_id']);
            if ($user) return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function isValidCourse(string $str, string $fields, array $data)
    {
        try {
            $model = new Courses();
            $course = $model->findCourseById($data['course_id']);
            if ($course) return true;
        } catch (Exception $e) {
            return false;
        }
    }

    //batas
}
